# Descrição do Projeto
Projeto desenvolvido com base no curso "Angular 6 e 7, Apollo, GraphQL e Graphcoo"  da udemy.



# AngularGraphcoolChat

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

## Apolo
###Apollo Client:
 npm i -S -E apollo-angular@1.5.0 apollo-angular-link-http@1.4.0 apollo-client@2.4.6 apollo-cache-inmemory@1.3.10 graphql-tag@2.9.2

### Tratamento de erros
npm i -S -E apollo-link@1.2.2 apollo-link-error@1.0.9

###Auth
gcf add-template auth/email-password

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Plugins
ng add @angular/material@6.0.1

npm i hammerjs


## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
