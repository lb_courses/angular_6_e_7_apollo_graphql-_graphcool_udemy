import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule, MatToolbarModule, MatFormFieldModule, MatInputModule, MatButtonModule, MatSnackBarModule, MatProgressSpinnerModule, MatSlideToggleModule } from '@angular/material'
import { ReactiveFormsModule } from '@angular/forms';
@NgModule({
  exports: [
    // CommonModule, exporta os recursos mais comuns
    CommonModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatToolbarModule,
    //formularios
    ReactiveFormsModule,
    // exibe mensagens pro user
    MatSnackBarModule,
    // spinner
    MatProgressSpinnerModule,
    MatSlideToggleModule

  
  ],
  declarations:[]
})
export class SharedModule {  }