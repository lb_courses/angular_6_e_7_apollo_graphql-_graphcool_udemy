import { NgModule, Inject } from '@angular/core';

import { ApolloModule, Apollo } from 'apollo-angular';
// facilita as requisições
import { HttpLinkModule, HttpLink } from 'apollo-angular-link-http';
import { HttpClientModule, HttpHeaders } from '@angular/common/http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { environment } from 'src/environments/environment.prod';
import { onError } from 'apollo-link-error';
import { ApolloLink } from 'apollo-link';
import { StorageKeys } from './storage-keys';
import { GRAPHCOOL_CONFIG, GraphcoolConfig } from './core/providers/graphcool-config.provider';


@NgModule({
    imports: [
        // ele que fornece o httpClient pro httpLink
        HttpClientModule,
        ApolloModule,
        HttpLinkModule
    ]
})
export class ApolloConfigModule {

    constructor(
        private apollo: Apollo ,
        private httpLink: HttpLink ,
        @Inject(GRAPHCOOL_CONFIG) private graphcoolConfig : GraphcoolConfig
    ){

        const authWiddleware : ApolloLink = new ApolloLink( (operation , forward) => {            
            // adiciona um cabeçalho na requisição http
            
            console.log('Context' , operation.getContext());
            operation.setContext({
                // headers: {
                //     'Authorization' : `Bearer ${this.getAuthToken()}`
                // }
                headers: new HttpHeaders({
                    'Authorization' : `Bearer ${this.getAuthToken()}`
                })
            });
            
            console.log('Context' , operation.getContext());
            return forward(operation);
        }  );

        // cria um link que permite que envia pro graphql
        // const uri = 'https://api.graph.cool/simple/v1/cjs7xpzd70uyv0100xmj3g0t0';
        const uri = this.graphcoolConfig.simpleAPI;
        // altera a url da api
        const http = httpLink.create( 
            // usa nossa uri
            { uri }
            );

        //tratamento de erros
        
        const linkError = onError(({ graphQLErrors, networkError }) => {
            if (graphQLErrors){
            graphQLErrors.map(({ message, locations, path }) =>
                console.log(
                `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
                ),
            );
            }
            if (networkError){ console.log(`[Network error]: ${networkError}`);  }
        });

        apollo.create({
            // link: http,
            //cria uma conposição de links
            link: ApolloLink.from([
                linkError,
                authWiddleware.concat(http)
            ]),
            cache: new InMemoryCache(),
            // epenas em prod
            connectToDevTools: environment.production
        });
    }

    private getAuthToken(): string {
        return window.localStorage.getItem(StorageKeys.AUTH_TOKEN);
    }

}