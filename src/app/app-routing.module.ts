import { NgModule } from '@angular/core';
import { RouterModule , Routes } from '@angular/router';

const routes : Routes = [
    // full, caminho tem que ser igual o da rota
    // prefix, começa com o path
    { path : '' , redirectTo: 'login' , pathMatch: 'full'}   
];

@NgModule({
    imports: [
        // chama apenas uma vez na aplicação
        RouterModule.forRoot(routes)
    ],
    exports: [ RouterModule ]
})

export class AppRoutingModule {

}