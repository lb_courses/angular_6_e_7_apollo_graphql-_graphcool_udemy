// import { BrowserModule } from '@angular/platform-browser';
// import { MatToolbarModule, MatListModule } from '@angular/material';
// import { ApolloConfigModule } from './apollo-config.module';
import { NgModule } from '@angular/core';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';



// import { HttpClientModule } from '@angular/common/http';

import { CoreModule } from './core/core.module';
import { LoginModule } from './login/login.module';
import { AppRoutingModule } from './app-routing.module';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    // BrowserModule,    
    // MatToolbarModule,
    // MatListModule,
    // ApolloConfigModule,
    // BrowserAnimationsModule,    
    // HttpClientModule,
    CoreModule,
    LoginModule,
    AppRoutingModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
