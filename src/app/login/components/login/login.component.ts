import { Component, OnInit, OnDestroy, HostBinding } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { validate } from 'graphql';
import { AuthService } from 'src/app/core/services/auth.service';
import { Observable } from 'rxjs';
import { takeWhile } from 'rxjs/operators';
import { ErrorService } from 'src/app/core/services/error.service';
import { MatSnackBar } from '@angular/material';

// ng g c login/components/login 

@Component({
  // selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit , OnDestroy{

  loginForm: FormGroup;

  configs =  {
    isLogin : true ,
    actionText: 'SignIn',
    buttonActionText: 'Create Account',
    isLoading : false
  };

  // caso esteja tentando fazer o cadastro.
  // tem o campo name.
  private nameControl = 
    new FormControl('' , [Validators.required , Validators.minLength(5)]);

    // variavel para parar o subscribe quando for para outra page
  private alive = true;

  // usamos isso pois tiramos o selector do login
  // add a class css no spinner
  @HostBinding('class.app-login-spinner') private appySpinnerClass = true;
  
  constructor(
    public authService : AuthService,
    private errorService : ErrorService,
    private formBuilder : FormBuilder,
    // exibe msgs pro user
    private snacBar : MatSnackBar
  ) { }

  ngOnInit() {
    this.createForm();


    const userData = this.authService.getRememberMe();
    if( userData ) {
      this.email.setValue(userData.email);
      this.email.setValue(userData.password);
    }

  }


  createForm():void {
    this.loginForm = this.formBuilder.group({
      email : ['' , [Validators.required , Validators.email] ],
      password : ['' , [Validators.required , Validators.minLength(5)] ]
    });
  }

  onSubmit() {
    console.log(this.loginForm.value);

    this.configs.isLoading = true;

    // const operation : Observable<any> =
    const operation  =
      (this.configs.isLogin)
      // se for login
        ? this.authService.signinUser(this.loginForm.value)
        // se for cadastro
        : this.authService.signupUser(this.loginForm.value);

        operation
        // para o subscribe
        .pipe(
          takeWhile( () => this.alive )
        )
        .subscribe( res => {
            console.log('redirecting...' , res);
          

            this.authService.setRememberMe(this.loginForm.value);
            const redirect : string = this.authService.redirectUrl || '/dashboard';
            //redirect with router
            console.log("route to redirect" , redirect);
            this.authService.redirectUrl = null;
            this.configs.isLoading = false;
        } ,
         error => {
          this.configs.isLoading = false;
          //  this.errorService.getErrorMessage(error);
            console.log(this.errorService.getErrorMessage(error));
            this.snacBar.open( this.errorService.getErrorMessage(error) , 'Done' , {
              duration: 2000 ,
              verticalPosition : 'top'
            } )
         } , () => console.log("observable completed"));

  }


  changeAction() : void {
    this.configs.isLogin  = !this.configs.isLogin;
    // se n tiver fazendo login , !this.configs.isLogin
    this.configs.actionText = !this.configs.isLogin ? 'SignUp' : 'SignIn'; 
    // se tiver tentando fazer o login e não tiver conta pode criar uma
    // se tiver tentando criar, e já tem um login.
    this.configs.buttonActionText = !this.configs.isLogin ? 'Already have account' : 'Create account'; 

    // se não for o login ,add o campo name no form
    // se não tiver fazendo o cadastro remove o campo name do form
    !this.configs.isLogin ? this.loginForm.addControl('name' , this.nameControl) : this.loginForm.removeControl('name');
  }

  get email() : FormControl {
    // this.loginForm.get('email') é um FormBuilder
    return <FormControl> this.loginForm.get('email');
  }

  get password() : FormControl {
    // this.loginForm.get('email') é um FormBuilder
    return <FormControl> this.loginForm.get('password');
  }

  get name() : FormControl {
    // this.loginForm.get('email') é um FormBuilder
    return <FormControl> this.loginForm.get('name');
  }

  // metodo para parar o subscribe , medida de cautela


  onKeepSigned() : void {
    this.authService.toggleKeepSigned();
  }

  onRemeberMe(): void {
    this.authService.toogleRememberMe();
  }

  ngOnDestroy():void {
      this.alive = false;
  }

}
