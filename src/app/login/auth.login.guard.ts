import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, Router } from '@angular/router';
import { Observable , of  } from 'rxjs';
import { AuthService } from '../core/services/auth.service';
import { map, tap } from 'rxjs/operators';


@Injectable({providedIn: 'root'})
export class AutoLoginGuard implements CanActivate {
    constructor(
        private authService : AuthService,
        private router : Router
    ) { }

    canActivate(route: ActivatedRouteSnapshot, 
        state: RouterStateSnapshot): Observable<boolean> {
        // console.log("AuthLoginGuard");
        // return this.authService.isAuthentucated
        // .pipe(
        //     tap( is => {
        //         if( is ) {
        //             // se estiver autenticado
        //             this.router.navigate(['/dashbord']);
        //         }
        //     }),
        //     map( is => !is )
        // ) ;

        return of(true);
    }
}