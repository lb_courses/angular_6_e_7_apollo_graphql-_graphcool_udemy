import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { AutoLoginGuard } from './auth.login.guard';

// ng g m login --routing 

const routes: Routes = [
  // o nome da path tem que ser o mesmo no app-routing
  { path: 'login' , component: LoginComponent , canActivate: [AutoLoginGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [ AutoLoginGuard]
})
export class LoginRoutingModule { }
