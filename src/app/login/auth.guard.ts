import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, CanActivateChild, CanLoad, Route, Router } from '@angular/router';
import { LoginRoutingModule } from './login-routing.module';
import { AuthService } from '../core/services/auth.service';
import { Observable, pipe } from 'rxjs';
import { take, tap } from 'rxjs/operators';

// import { stat } from 'fs';


// para guardas de pagina
// tem que fornecer em algum serviço
@Injectable({ providedIn: LoginRoutingModule })
export class AuthGaurd implements CanActivate, CanActivateChild, CanLoad {
    
    constructor(
        private router : Router,
        private authService: AuthService) {}

    // ver se tem acesso para determinada rota
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        // return true;
        // return this.authService.isAuthentucated;
        return this.chackAuthState(state.url);
    }

    // rotas filhas
    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | import("@angular/router").UrlTree | Observable<boolean | import("@angular/router").UrlTree> | Promise<boolean | import("@angular/router").UrlTree> {
        return this.canActivate( route , state );
    }
    
    // o Observable no canLoad precisa completar
    // canLoad(route : Route) : Observable<boolean> {
    canLoad(route: import("@angular/router").Route, segments: import("@angular/router").UrlSegment[]): boolean | Observable<boolean> | Promise<boolean> {
       
        // route.path está com bug 

        // essa forma pode dar problema no lazyload
        const url = window.location.pathname;

        // throw new Error("Method not implemented.");
        // return this.authService.isAuthentucated
        return this.chackAuthState(url)
        // take(1), um numero de next que o observable emit
        .pipe( take(1) );
    }
    
    private chackAuthState(url : string): Observable<boolean>{
        return this.authService.isAuthentucated
        .pipe( 
            tap(is => {
                if( !is ) {
                    this.authService.redirectUrl = url;
                    // se não tiver auth leva para o login
                    this.router.navigate(['/login']);
                }
            })
        );
    }
}