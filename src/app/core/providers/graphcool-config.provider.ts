import { InjectionToken } from '@angular/core';

const graphcoolId = 'cjs7xpzd70uyv0100xmj3g0t0';

// interfaces só são vista pelo ts
export interface GraphcoolConfig {
    simpleAPI : string;
    subscriptionsAPI: string;
    fileAPI : string;
    fileDownloadURL: string;
}


export const graphcoolConfig : GraphcoolConfig = {
    simpleAPI: `https://api.graph.cool/simple/v1/${graphcoolId}`,
    subscriptionsAPI: ` https://api.graph.cool/relay/v1/${graphcoolId}`,
    fileAPI: ` https://api.graph.cool/file/v1/${graphcoolId}`,
    fileDownloadURL: `https://files.graph.cool/${graphcoolId}`
};

// InjectionToken permite trabalhor com tipos genericos
// previne a colisão de depedencias
export const GRAPHCOOL_CONFIG = new InjectionToken<GraphcoolConfig>(
    `graphcool.config`,{
        providedIn : 'root' ,
        factory : () => {
            return graphcoolConfig;
        }
    }
);