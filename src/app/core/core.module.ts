import { NgModule, Optional, SkipSelf } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
// import { MatToolbarModule, MatListModule } from '@angular/material';
import { ApolloConfigModule } from '../apollo-config.module';

// tira os modulos do app.component

@NgModule({
  exports: [
    BrowserModule,    
    // MatToolbarModule,
    // MatListModule,
    ApolloConfigModule,
    BrowserAnimationsModule,

  ]
})
export class CoreModule { 

  // não pode se importado por modulos de recusos
  constructor(
    // verifica se já fez o import dele em mais de um module
    // quando tenta importa dentro de outro modulo
    // se já tiver importado da um erro
    //@Optional(), indica que essa dependencia n exista
    //@SkipSelf, fala pro angular ignorar dependencia que estão no mesmo nivel
    @Optional() @SkipSelf() parentModule: CoreModule
  ){
    
    // lança o erro;
    if( parentModule  ) {
      throw new Error('CoreModel is already loaded. Import it in the AooModule only.');
    }

  }

}
