import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor() { }


  getErrorMessage( error : Error ) : string {
    // quebra a mensagem
    const message  = error.message.split(': ');
    // message = message[message.length - 1];      
      return message[message.length - 1];
  }

}
