import { Injectable } from '@angular/core';
import {  of , Observable , Subject, BehaviorSubject, ReplaySubject, throwError } from 'rxjs';
import { Apollo } from 'apollo-angular';
import { AUTHENTICATE_USER_MUTATION, SIGNUP_USER_MUTATION, LoggedInUserQuery, LOGGED_IN_USER_QUERY } from './auth.graphql';
import { map, tap, catchError, mergeMap } from 'rxjs/operators';
import { StorageKeys } from 'src/app/storage-keys';
import { Router } from '@angular/router';
import { Base64 } from  'js-base64';

// import { of } from 'zen-observable';

// npm i -E js-base64@2.4.5
// npm i -D @types/js-base64@2.3.1

@Injectable({
  // agora pode prover o serviço dentro dele mesmo
  // providedIn: 'root' , injeta na raiz da aplicação
  providedIn: 'root'
})
export class AuthService {

  redirectUrl : string;
  keepSigned : boolean;
  rememberME: boolean;

  /*
  Subject
  BehaviorSubject
  ReplaySubject
  */

  // guardas de paginas
  // private _isAuthentucated = new Subject <boolean>();
  // private _isAuthentucated = new BehaviorSubject <boolean>( false );

  // (1) numero de valores que ele vai guarda
  private _isAuthentucated = new ReplaySubject <boolean>(1);



  constructor(
    private apollo : Apollo   ,
    private router : Router 
    ) {      

      this.isAuthentucated.subscribe( 
        is => console.log("AuthState" , is)
       );

       this.init();

      //  this.validateToken()
      //   .subscribe(
      //     res => console.log('Res' , res),
      //     err => console.log('Res' , err),
      //   );

      // testes
      // // guard de paginas
      // // aguarda o NEXT ser chamado
      // this.isAuthentucated.subscribe( res => {
      //   console.log("authState" , res);
      // } );      

      // let authState = false;

      // setInterval( ( ) => {

      //    this._isAuthentucated.next(authState);
      //    authState = !authState;
      // } , 5000 );

   }

   init() : void {
     this.keepSigned = JSON.parse( window.localStorage.getItem( StorageKeys.KEEP_SIGNED ) );
     this.rememberME = JSON.parse( window.localStorage.getItem( StorageKeys.REMEMBER_ME ) );
   }

  // guard de paginas
  //  retorna ele assim para que outros componentes não o altere
   get isAuthentucated(): Observable<boolean> {
     return this._isAuthentucated.asObservable();
   }

  signinUser( variables: { email : string , password : string } ): Observable <{id: string , token : string}> {
      return this.apollo.mutate({
        mutation : AUTHENTICATE_USER_MUTATION ,
        variables
      }).pipe(
        map( res => res.data.authenticateUser),
        
        // pega o valor que o map retorna
        // tap( res => this.setAuthState( res!= null )),
        tap( res => this.setAuthState( {token : res && res.token , isAuthentucated : res !== null } )),
        // pega o erro
        catchError( error => {
          // this.setAuthState(false);
          this.setAuthState({ token : null , isAuthentucated : false });
          return throwError(error);
        } ) );
  }

  signupUser( variables: { name:string , email : string , password : string } ): Observable <{id: string , token : string}> {
    return this.apollo.mutate({
      mutation : SIGNUP_USER_MUTATION ,
      variables
    }).pipe(
      map( res => res.data.signupUser),
      // pega o valor que o map retorna
      // tap( res => this.setAuthState( res!= null )),
      tap( res => this.setAuthState( {token : res && res.token , isAuthentucated : res !== null } )),
       // pega o erro
       catchError( error => {
        // this.setAuthState(false);
        this.setAuthState({ token : null , isAuthentucated : false });
        return throwError(error);
      } ) );
    

  }

  toggleKeepSigned() : void {
    this.keepSigned = !this.keepSigned;
    window.localStorage.setItem( StorageKeys.KEEP_SIGNED , this.keepSigned.toString() );
  }

  toogleRememberMe() : void {
    this.rememberME =  !this.rememberME;
    window.localStorage.setItem(StorageKeys.REMEMBER_ME , this.rememberME.toString());
    if( !this.rememberME ) {
      window.localStorage.removeItem(StorageKeys.USER_EMAIL);
      window.localStorage.removeItem(StorageKeys.USER_PASSWORD);
    }
  }

  setRememberMe( user : { email : string , password : string }  ) : void  {
    if( this.rememberME ) {
      window.localStorage.setItem(StorageKeys.USER_EMAIL , Base64.encode(user.email));
      window.localStorage.setItem(StorageKeys.USER_PASSWORD , Base64.encode(user.password));
    }
  }

  getRememberMe():  { email : string , password : string }  {
      if( !this.rememberME ) { return null;}

      return {
        email : Base64.decode(window.localStorage.getItem(StorageKeys.USER_EMAIL)),
        password: Base64.decode(window.localStorage.getItem(StorageKeys.USER_EMAIL))
      };

  }

  

  // salvar os dados


  // busca os dados

  logout() : void {
    window.localStorage.removeItem(StorageKeys.AUTH_TOKEN);
    window.localStorage.removeItem(StorageKeys.KEEP_SIGNED);
    this.keepSigned = false;
    this._isAuthentucated.next(false);
    this.router.navigate(['/login']);
    // reseta o chach do apollo 
    this.apollo.getClient().resetStore();
  }

  autoLogin():Observable<void> {
    // escolheu se mantar logado
    if( !this.keepSigned ) {
        this._isAuthentucated.next(false);
        window.localStorage.removeItem(StorageKeys.AUTH_TOKEN);
        return of();
    }

    return this.validateToken()
      .pipe(
        tap( authData => {
          const token  = window.localStorage.getItem( StorageKeys.AUTH_TOKEN);
          this.setAuthState({ token , isAuthentucated : authData.isAuthenticated });
        } ),
        mergeMap(res => of()),
        catchError( error =>  {
          // quando tenta entrar e o servidor da erro
          this.setAuthState( { token : null , isAuthentucated : false });
          return throwError(error);
        } ) 

      );
  }

  private validateToken() : Observable<{id: string , isAuthenticated: boolean}> {
    return this.apollo.query<LoggedInUserQuery>({
      query : LOGGED_IN_USER_QUERY
    }).pipe(
      map( res => {
        const user = res.data.loggedInUser;
        return {
          id : user && user.id ,
          isAuthenticated : user !== null
        }
      })
    );
  }

  // informa se o user esta auth ou não
  private setAuthState( authData: { token : string ,  isAuthentucated : boolean  } ) : void {
    if( authData.isAuthentucated ) {
      // salva o token no storage
      window.localStorage.setItem( StorageKeys.AUTH_TOKEN , authData.token );
    }
    this._isAuthentucated.next(authData.isAuthentucated);
  }

}
