import { Component, OnInit } from '@angular/core';
// import { HttpClient } from '@angular/common/http';
import { Apollo } from 'apollo-angular';

import gql  from 'graphql-tag';
import { AuthService } from './core/services/auth.service';
import { take } from 'rxjs/operators';
import { ErrorService } from './core/services/error.service';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-root',
  template: `
  <router-outlet></router-outlet>
  `
  // template: `  
  // <app-login></app-login>
  // `
  // templateUrl: './app.component.html',
  // styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit{

  constructor(
    private authService: AuthService,
    private errorService : ErrorService,
    private snackBar : MatSnackBar
  ){
  }

  ngOnInit(): void {
    this.authService.autoLogin()
    .pipe(take(1))
      .subscribe(
        null ,  
        error => {
          const message =  this.errorService.getErrorMessage(error);
          this.snackBar.open(`Unexpected error: ${message}`),
          'Done',
          { duration : 5000 , verticalPosition : 'top'}
           
        }
      );
  }




}

// export class AppComponent {
//   title = 'angular-graphcool-chat';

//   // private apiURL = 'https://api.graph.cool/simple/v1/cjs7xpzd70uyv0100xmj3g0t0';
//   constructor(
//     private apollo : Apollo,
//     // private http : HttpClient
//   ){
//     // this.createUser();
//     this.allUsers();
//   }

//   allUsers():void {

//     // const body = {
//     //   query: `query{
//     //             allUsers{
//     //               id
//     //               name
//     //             }
//     //           }`
//     // };

//     // this.http.post( this.apiURL , body )
//     // .subscribe(res => console.log('query = ' , res) );

//     this.apollo.query({
//       // gql recebe uma uma funcao
//       query: gql`
//           query{
//             allUsers{
//               id
//               name
//            }
//           }
//       `
//     }).subscribe( res =>  console.log('query = ' , res ));

//   }

//   createUser(): void {
//     // const body = {
//     //   query : 
//     //   `mutation  CreateNewUser( $name: String! , $email: String! , $password: String! ){  
//     //     createUser(
//     //       name: $name ,
//     //       email: $email ,
//     //       password: $password
//     //     ){
//     //       id
//     //       name
//     //       email
//     //     }
//     //   }`,
//       // variables: {
//       //   name : 'mila' ,
//       //   email : 'mila@gmail.com' ,
//       //   password: 'milinha'
//       // }
//     // }

//     // this.http.post( this.apiURL , body )
//     // .subscribe(res => console.log('mutation = ' , res) );

//     this.apollo.mutate( {
//       mutation: gql`

//   mutation  CreateNewUser( $name: String! , $email: String! , $password: String! ){  
//         createUser(
//           name: $name ,
//           email: $email ,
//           password: $password
//         ){
//           id
//           name
//           email
//         } 
//       }     
//       `,
//       variables:{      
//         name : 'ana' ,
//         email : 'ana@gmail.com' ,
//         password: 'ana'
//       }
//     } ).subscribe(res => console.log('mutation = ' , res) );

//   }

// }
