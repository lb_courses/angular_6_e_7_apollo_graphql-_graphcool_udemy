// fala se o usuário optou ou nao por ficar autenticado
export class StorageKeys {
    static KEEP_SIGNED = 'agc-keep-signeg';
    static AUTH_TOKEN = 'agc-auth-token';

    static REMEMBER_ME = 'agc-remember-me';
    static USER_EMAIL = 'agc-user-email';
    static USER_PASSWORD = 'agc-user-password';
}
